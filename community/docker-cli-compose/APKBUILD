# Contributor: Jake Buchholz Göktürk <tomalok@gmail.com>
# Maintainer: Jake Buchholz Göktürk <tomalok@gmail.com>
pkgname=docker-cli-compose
pkgver=2.11.2
pkgrel=0
pkgdesc="A Docker CLI plugin for extended build capabilities"
url="https://docs.docker.com/compose/cli-command"
arch="all"
license="Apache-2.0"
depends="docker-cli"
makedepends="go"
options="net"
source="compose-$pkgver.tar.gz::https://github.com/docker/compose/archive/v$pkgver.tar.gz"

_plugin_installdir="/usr/libexec/docker/cli-plugins"

builddir="$srcdir"/compose-"$pkgver"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	PKG=github.com/docker/compose/v2
	local ldflags="-X '$PKG/internal.Version=v$pkgver'"
	go build -modcacherw -ldflags="$ldflags" -o docker-compose ./cmd
}

check() {
	# e2e tests are excluded because they depend on live dockerd/kubernetes/ecs
	local pkgs="$(go list -modcacherw ./... | grep -Ev '/e2e(/|$)')"
	go test -modcacherw -short $pkgs
	./docker-compose compose version
}

package() {
	install -Dm755 docker-compose "$pkgdir$_plugin_installdir"/docker-compose
}

sha512sums="
174ef8e38dce65f6ce117f8efdd24049dd9e56aa7c619242b4514ba258461ec1d65b2d226601a6efb3caf7b8516ba5c65af48854f70a2eff25adcdd3f9b9f697  compose-2.11.2.tar.gz
"
