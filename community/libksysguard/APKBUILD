# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libksysguard
pkgver=5.26.0
pkgrel=0
pkgdesc="KDE system monitor library"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="LGPL-2.1-or-later AND (GPL-2.0-only OR GPL-3.0-only)"
depends_dev="
	kauth-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knewstuff-dev
	kservice-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	libcap-dev
	libnl3-dev
	libpcap-dev
	lm-sensors-dev
	plasma-framework-dev
	qt5-qttools-dev
	qt5-qtwebchannel-dev
	zlib-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/libksysguard-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

replaces="ksysguard<5.22"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# processtest requires working OpenGL
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "processtest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
fc459a1f640191644b6efc92e3a5981e380b113c0f763b770aef0b192a8e5defc5d1e56acc537acc4b107b3b255acf75ebbce57cc36e8219a854e10beaeced46  libksysguard-5.26.0.tar.xz
"
