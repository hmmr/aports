# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=ruby-bundler
_gemname=bundler
pkgver=2.3.23
pkgrel=0
pkgdesc="Manage an application's gem dependencies"
url="https://bundler.io/"
arch="noarch"
license="MIT"
depends="ruby"
makedepends="ruby-rake"
subpackages="$pkgname-doc"
source="https://github.com/rubygems/rubygems/archive/bundler-v$pkgver.tar.gz
	manpages.patch
	"
builddir="$srcdir/rubygems-bundler-v$pkgver/bundler"
options="!check"  # tests require deps not available in main repo

build() {
	rake build_metadata
	gem build $_gemname.gemspec
}

package() {
	local gemdir="$pkgdir/$(ruby -e 'puts Gem.default_dir')"

	gem install \
		--local \
		--install-dir "$gemdir" \
		--bindir "$pkgdir/usr/bin" \
		--ignore-dependencies \
		--no-document \
		--verbose \
		$_gemname

	local n; for n in 1 5; do
		mkdir -p "$pkgdir"/usr/share/man/man$n
		mv "$gemdir"/gems/$_gemname-$pkgver/lib/bundler/man/*.$n "$pkgdir"/usr/share/man/man$n/
	done

	rm -rf "$gemdir"/cache \
		"$gemdir"/build_info \
		"$gemdir"/doc \
		"$gemdir"/gems/$_gemname-$pkgver/man \
		"$gemdir"/gems/$_gemname-$pkgver/*.md
}

sha512sums="
89367d04e0bd89d4ccfe2b003b839f84899080642d2909874b7a2f6837a3b26b4c7fc54d46fcb42ae3f37afb328abff49a9bb7c42d9644c10d88ba366b1ca359  bundler-v2.3.23.tar.gz
77a36e61ed205aeea6114b1039dfbe29fcaf916eeae3f91785aa53b3ac534e004aa257e218534d927f39e3673eebbfb3ef9ee17f04ed81f74117799b88e53cf4  manpages.patch
"
